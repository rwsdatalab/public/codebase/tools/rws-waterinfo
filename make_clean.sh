echo "Cleaning repo first.."
rm -rf dist
rm -rf build
rm -rf rws_waterinfo.egg-info
rm -rf __pycache__
rm -rf .pytest_cache
rm -rf rws_waterinfo/__pycache__
rm -rf rws_waterinfo/.pylint.d
rm -rf rws_waterinfo/tests/__pycache__
rm -rf tests/__pycache__
rm -rf rws_waterinfo/utils/__pycache__
rm -rf .pylint.d
rm -rf rws_waterinfo/data/*.zip
rm -rf rws_waterinfo/data/*.csv
rm -rf *.js
rm -rf *.html
rm -rf *.css
rm -rf *.dot
rm -rf *.png
rm -rf .coverage*
rm -rf .pkl
rm -rf rws_waterinfo.egg-info
rm -rf .mypy_cache
rm -rf data/*
