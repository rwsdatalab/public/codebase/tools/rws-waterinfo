Core Functionalities
--------------------

Get catalog
***********


.. automodule:: rws_waterinfo.rws_waterinfo.get_catalog
    :members:
    :undoc-members:
    :noindex:



Get data
********

.. automodule:: rws_waterinfo.rws_waterinfo.get_data
    :members:
    :undoc-members:
    :noindex:


Update Dataframe
****************

.. automodule:: rws_waterinfo.rws_waterinfo.update_dataframe
    :members:
    :undoc-members:
    :noindex:
