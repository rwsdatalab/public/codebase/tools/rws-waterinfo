.. rws_waterinfo documentation master file

rws_waterinfo's documentation
================================

**Date**: 14-03-2023 **Version**: 0.1.0

See this document as `pdf <rws_waterinfo.pdf>`_.

rws_waterinfo is an open source wrapper for downloading observation data from waterinfo.rws.nl.

.. toctree::
   :maxdepth: 1
   :caption: Getting started

   Installation <installation.rst>
   Core Functionalities <Core Functionalities.rst>

.. toctree::
   :maxdepth: 1
   :caption: All the rest

   API <apidocs/rws_waterinfo.rst>
   Contributing <contributing.rst>
   License <license.rst>
   Release notes <changelog.rst>

Getting help
------------

Having trouble? We'd like to help!

- Looking for specific information? Try the :ref:`genindex` or :ref:`modindex`.
- Report bugs with rws_waterinfo in our `issue tracker <https://gitlab.com/rwsdatalab/public/codebase/tools/rws_waterinfo/-/issues>`_.
