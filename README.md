# rws_waterinfo

## What is it?
rws_waterinfo is a wrapper for downloading observation data from the waterinfo api. It simplifies the proces to make requests.

To clone rws-waterinfo:

git clone https://gitlab.com/rwsdatalab/public/codebase/tools/rws-waterinfo.git

To install rws_waterinfo:

pip install rws-waterinfo
