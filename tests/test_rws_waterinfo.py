# -*- coding: utf-8 -*-

"""Tests for the example module."""

import pytest
import pandas as pd
import rws_waterinfo as rw

# Initialize rws_waterinfo library


def test_get_catalog():
    catalog = rw.get_catalog()
    assert isinstance(catalog, pd.DataFrame)
    assert catalog.shape[0] > 0


# def test_with_error():
#     with pytest.raises(ValueError):
#         # Do something that raises a ValueError
#         raise (ValueError)


# Fixture example
@pytest.fixture
def an_object():
    return {}


def test_example(an_object):
    assert an_object == {}
